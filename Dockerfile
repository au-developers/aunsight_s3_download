FROM registry.aunalytics.com/au/python_lib_aunsight:3.6-slim
# When not using Aunsight logger you can use py slim image
# FROM python:3.7.4-slim

# Need to set the timezone or date/time functions will give you UTC
ENV TZ=America/Indiana/Indianapolis
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
# Pulled this from SAP repo - no clue why all of these are needed
RUN apt-get -yq update \
    && apt-get upgrade -y \
# Pulled this from SAP repo - no clue why all of these are needed
    && apt-get install -y gcc procps curl build-essential \
        unixodbc-dev libaio1 libaio-dev libssl-dev openssl python-openssl \
        # libpq-dev needed for pyscopg2 (redshift)
        libpq-dev \
        # Need these for adobe
        g++ \
        # Only installing git to use with vscode remote container
        git \
    && apt-get clean all 
# Need these env vars to run sapgenpse
# Run this first to use cached layers when testing
COPY requirements.txt workdir/
RUN pip install -r workdir/requirements.txt
COPY . /workdir
WORKDIR /workdir
ENTRYPOINT ["/bin/bash"]
CMD ["main.sh"]