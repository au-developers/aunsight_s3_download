# How to build
`docker build -t aunsight_s3_download .`

# How to upload to Aunsight (if testing connectivity)
* Save container to stdout or file
`docker save aunsight_s3_download | au2 process version upload --process <process id> --ver <version number> --stdin --wait`

* Set proper context in au2 and grab proc id

* All commands
```shell
docker build -t aunsight_s3_download .
docker run -e LOG_LEVEL=DEBUG  aunsight_s3_download
```
