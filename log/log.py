import logging
import sys
import os

def logger_setup(logger_name):
    '''Sets up logger for a given name (usually __name__) and return a logger
    
    Args:
        logger_name (str): Logger name
    
    Returns:
        obj: logger obj
    '''
    # If there's an Aunsight token env var, then use the Aunsight logger
    if os.getenv("AU_CONTEXT_TOKEN"):
        logger = get_au_logger(logger_name)
        au_logger = True
    # If not, just use a normal logger
    else:
        logger = logging.getLogger(logger_name)
        logger.addHandler(logging.StreamHandler(sys.stdout))
        LOG_LEVEL = os.getenv('LOG_LEVEL','INFO').strip().upper()
        logger.level = getattr(logging,LOG_LEVEL)
        au_logger = False
    return logger, au_logger

def get_au_logger(name="main"):
    from lib_aunsight.context import AunsightContext
    from lib_aunsight.lib.util.Logger import Logger
    # Aunsight specific info
    token = os.environ["AU_CONTEXT_TOKEN"]
    LOG_LEVEL = os.getenv('LOG_LEVEL','INFO').strip().upper()
    organization = os.environ["AU_ORGANIZATION"]
    context = AunsightContext(token=token)
    stream_id = os.getenv("AU_LOGGER_STREAM")
    return Logger(
        context=context,
        stream=stream_id,
        stdout=True,
        name=name,
        level=LOG_LEVEL,
        throw_on_error=True
    )