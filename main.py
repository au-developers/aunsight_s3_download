import boto3, botocore
from lib_aunsight.context import AunsightContext as AUcontextMethod
from lib_aunsight.lib.util.Logger import Logger as AUlogger
from lib_aunsight import context, lib
from log.log import logger_setup
logger,au_logger = logger_setup(__name__)

def au_create_atlas_id(my_context):
    logger.info('Creating atlas record')
    record = my_context.atlas_record()
    record.set('name', 'temp_rec')
    record.set('organization', '6efba1cc-94d0-4965-ae10-063868002181')
    record.set('location.resource', 'v2:93b56c0e-58e7-4140-bde9-d68a812b1b6a')
    record.set('location.auto_path', True)
    record.set('format', 'csv')
    record.set('type', 'source')
    record.create()
    logger.info('Atlas id created with id:'+record.id)
    return record,record.id

def download_s3_files():
    logger.info('Downloading files from S3')
    public_key = str(lib.models.Secret(my_context, '4f1b7ed4-a6db-4daf-92eb-387319d7b785').download().text)
    secret_key = str(lib.models.Secret(my_context,'6d2e158c-f71e-4334-9135-d51499f848e1').download().text)
    session = boto3.Session(aws_access_key_id=public_key,aws_secret_access_key=secret_key)
    bucket_name='datalake-dev-s3-aunalytics'
    b = session.resource('s3').Bucket(bucket_name)
    file_list = []
    au_file_list = []
    file_prefix = 'edm/edm_adobeprodcat'
    for i in b.objects.filter(Prefix=file_prefix):
        file_list.append(str(i.key))
        au_file_list.append(str(i.key).split('/')[1])
    for i in range(len(file_list)):
        b.download_file(file_list[i], 'temp_files/'+file_list[i].split('/')[1])
        logger.info(file_list[i].split('/')[1]+' downloaded')
    logger.info('Downloaded files from S3')
    return au_file_list

if __name__ == '__main__':
    logger.info('Initiating the process')
    my_context = context.AunsightContext(token='446e0a2c-2f75-4fde-b519-d14d25d55c47')
    au_file_list = download_s3_files()
    for i in range(len(au_file_list)):
        au_record, au_record_id = au_create_atlas_id(my_context)
        logger.info('uploading '+au_file_list[i]+' to '+au_record_id)
        with open('temp_files/'+au_file_list[i], 'rb') as data:
            job = au_record.upload(data, write_mode='overwrite')
        if (au_file_list[i] == 'edm_adobeprodcat_2020-11-06.csv'):
            logger.info('Copying '+au_file_list[i]+' data from '+au_record_id+' to 326dfdb8-0a27-4056-ace6-9bc77f9847a3')
            au_record.copy('326dfdb8-0a27-4056-ace6-9bc77f9847a3', write_mode='overwrite', interval=1)
        logger.info('Deleting atlas id: '+au_record_id)
        au_record.delete({"ignoreRecord":True})
        au_record.delete()
    logger.info('Process completed')
    logger.info('SUCCEEDED')

